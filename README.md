Le but des ces TP est de mettre en place un pipeline de d'intégration continue (Continuous Integration) qui permet d'intégrer le code des développeurs qui collaborent à un projet si leur code passe une série de tests.

Le TP1 permet de réaliser des tests sur un logiciel : tests unitaires, de couverture de code et de vérification de la conformité du programme aux spécifications d'un language (Java ici).

Le TP2 vise à automatiser le lancement des tests (via Gitlab CI/CD : https://docs.gitlab.com/ee/ci/)

Le TP3 va permettre à tous les collaborateurs du projet (qui développent une nouvelle fonctionnalités du programme dans une branche git) de tester leur code avant de demander au responsable du projet de fusionner leur branche avec la branche principale.


# TP1

Utilisation de techniques de debogage (tests unitaires, couverture de code et vérification de règles de codage).

## Tests unitaires

======== Tests unitaires =========

Le but de cette partie est d'écrire un programme de test pour la classe https://gitlab.com/BenOrcha/continuousintegration/blob/master/src/main/java/com/univ/Testing/Compte.java

Toutes les méthodes de cette classe doivent être testées.

Une ébauche du programme de test a tester est écrit dans la classe https://gitlab.com/BenOrcha/continuousintegration/blob/master/src/test/java/com/univ/Testing/TestingApplicationTests.java

Ce programmme utilise JUnit (voir le cours).

======== Téléchargement du projet =========

Utilisez la commande git clonee bouton dowload accessible Ã  partir de la page principale de ce projet pour tÃ©lÃ©charger ce projet, ou utilisez les commandes git.

======== Build du projet =========

Ce projet Java utilise Gradle, un outil qui permet de gÃ©rer les librairies telles que JUnit. Gradle va automatiquement tÃ©lÃ©charger les librairies utiles au projet. Attention cependant si l'accÃ¨s Ã  internet se fait via un proxy comme c'est le cas Ã  l'IUT. Dans ce cas il faut indiquer Ã  Gradle la configuration du proxy dans un fichier gradle.properties. D'oÃ¹ la prÃ©sence de ce fichier dans le projet. Si vous accÃ©dez directement Ã  Internet sans passer par un proxy (comme c'est le cas par exemple si vous utilisez le WiFi d'un tÃ©lÃ©phone portable) il faut supprimer le fichier gradle.properties du projet. 

Pour builder le projet il faut utiliser la commande 

./gradlew build			

sous Linux (attention ! parfois le fichier gradlew n'est pas exÃ©cutable. Dans ce cas, il faut exÃ©cuter la commande : chmod 777 gradlew)

ou

gradlew build			

sous Windows. 

Le premier lancement de cette commande prend du temps car un exÃ©cutable de Gradle est tÃ©lÃ©chargÃ©.

Cette commande commnence par compiler le code Java, lance automatiquement le programme de test. 

======== Consultation du rapport de test =========

Le build du projet s'est terminÃ© par le lancement automatique du programme de test JUnit. Le rapport HTML de test est accessible dans le dossier build/reports/tests/test. Il vous indique que les deux tests du programme de test ont rÃ©ussi. Ainsi il vous suffira de refaire la commande gradle build pour recompiler votre projet et relancer Ã  tout moment les tests.

======== Edition du projet =========

Vous pouvez utiliser n'importe quel �diteur de texte pour �diter les programmes (les erreurs de compilation apparaissent quand vous faites un build du projet.

======== Edition du projet sous Eclipse (optionnel et nÃ©cessite parfois un peu de configuration) =========

Avant d'Ã©diter le projet sous Eclipse, il faut convertir le projet gradle en un projet eclipse via la commande

./gradlew eclipse		

sous Linux ou

gradlew eclipse		

sous Windows.

Vous pouvez alors importer le projet sous Eclipse via le menu : File -> Import -> Existing project into workspage ...

Attention cependant, Eclipse va sous servir uniquement ��diter les programmes, mais pas effectuer les tests 
(ne tenez pas compte des erreurs de compilation qu'indique Eclipse mais utiliser la commande gradlew build pour voir les erreurs de compilation).

======== Codage =========

Terminer l'Ã©criture du programme https://gitlab.com/BenOrcha/continuousintegration/blob/master/src/main/java/com/univ/Testing/Compte.java 

Remarque : si vous ne vous sentez pas  Ã  l'aise avec les exceptions vous pouvez les effacer.

======== Ecriture du programme de test =========

ComplÃ©ter le programme de test https://gitlab.com/BenOrcha/continuousintegration/blob/master/src/test/java/com/univ/Testing/TestingApplicationTests.java 

afin de tester la classe de la classe de la question prÃ©cÃ©dente.

Lancer les tests via gradle build et corriger la classe a tester jusqu'Ã  ce qu'il n'y ait plus de bugs. Attention cependant ! si vous avez des erreurs de compilation, le build Ã©chouera. Un message d'erreur s'affichera. Vous devrez alors corriger le problÃ¨me de compilation avec que le build rÃ©ussisse.

## Tests de couverture de code

Le projet est configurÃ© pour effectuer des tests de couverture de code. 

Pour lancer les tests de couverture utilisez la commande : ./gradlew test jacocoTestReport

Le rapport de test HTML est dans le dossier build/jacocoHtml.

Jacoco vÃ©rifie la couverture de code C0 (instructions) et C1 (branches). VÃ©rifier que votre code est couvert Ã  100%. Si ce n'est pas la cas ajoutez des tests Ã  votre programme de test.

## Analyse statique du code source Java avec findBugs

findBugs est un outils qui permet de dÃ©tecter des erreurs de codage via une analyse statique des programmes (voir le cours).

Le lancement de findBugs se fait via la commande :

./gradlew findbugsMain

sous Linux, ou 

gradlew findbugsMain

sous Windows.

La rapport de test (en XML) est gÃ©nÃ©rÃ© dans le dossier build/reports/findbugs/main.xml.

L'analyse du rapport est un peu fastidieuse. Il faut repÃ©rer les balises BugInstance. VoilÃ  un exemple :

<BugInstance type="URF_UNREAD_FIELD" priority="2" rank="18" abbrev="UrF" category="PERFORMANCE">
  
Pour savoir Ã  quel bug cela correspond il faut chercher sur le site de findbugs : http://findbugs.sourceforge.net/bugDescriptions.html

En l'occurence le site indique :

UrF: Unread field (URF_UNREAD_FIELD)

This field is never read.  Consider removing it from the class.

On peut aussi choisir le niveau de seuil de findbugs. Pour fixer le seuil Ã  la valeur la plus stricte il faut changer la configuration de findbugs dans le fichier build.gradle comme suit :

findbugs {

	toolVersion = "3.0.1"

	effort = "max"
	
	reportLevel = "low"
	
}

# TP2

## Automatisation des tests

Le TP1 a permis de mettre en place des tests unitaires, des tests de couverture de code et des tests de conformité au languaga Java. 

Cependant le lancement de ces tests n'est pas automatique car il faut utiliser les commandes "gradlew build", "gradlew test jacocoTestReport" et "gradlew findbugsMain".

Or la création d'un pipeline d'intégration continue doit commencer par automatiser les tests.

Il existe de nombreux framework d'intégration continue. Ici on utilise Gitlab. 

Pour automatiser les tests, il faut définir dans un script les étapes à automatiser : https://gitlab.com/BenOrcha/continuousintegration/blob/master/.gitlab-ci.yml

Ce fichier définit 3 étapes : tests unitaires, tests de couverture de code et tests de conformité au languaga Java

Pour en savoir plus sur tel fichier : https://docs.gitlab.com/ee/ci/quick_start/README.html

Le lancement automatique du pipeline se fait dès qu'on push (via git push) une nouvelle version du code dans le repository de gitlab (voir le TP3). 
gitlab met automatique à disposition une machine et exécute le pipeline. 
On peut suivre le déroulement du pipeline dans https://gitlab.com/BenOrcha/continuousintegration/pipelines
 
# TP3

## CI (Continuous Integration) pipeline

![CI pileline](doc/cicd_pipeline_infograph.png)

Le but de ce TP est de lancer automatique le pipeline de test dès qu'on push (via git push) une nouvelle version du code dans le repository de gitlab.

Pour ce faire gitlab met automatique à disposition une machine et exécute le pipeline. 

On peut suivre le déroulement du pipeline dans https://gitlab.com/BenOrcha/continuousintegration/pipelines.

Pour cela il faut être membre du projet sur gitlab (developper). 
Il faut ensuite que le développeur créé une branche git sur sa propre machine et qu'il développe une nouvelle foncionnalité.
Il peut aussi faire des changements dans le programme principal afin d'intégrer ces nouvelles fonctionnalités : https://gitlab.com/BenOrcha/continuousintegration/blob/master/src/main/java/com/univ/Testing/TestingApplication.java
Au momment du build du projet (via gradlew build) une exécutable Java est généré dans le dossier build\libs du projet.
Le lancement du programme se fait via :

java -jar build/libs/....jar

sous Linux, ou 

java -jar build\libs\....jar

sous Windows.

Quand le développeur va chercher à pousser sa branche (git push) vers gitlab, le pipeline de test va être déclenché. 
Il pourra alors vérifier que son programme passe correctement des tests.

Si c'est bien le cas, il pourra envoyer une demande de fusion (merge request) de sa branche avec la branche principale.

# TP4

## CD (Continuous Delivery) ?

La livraison continue est la suite logique de l'intégration continue. Le but est de déployer automatiquement une application sur un serveur de test ou de production.

Or le présent projet se prête mal à se déploiement car il ne posssède pas d'interface web qui peut être déployée dans un serveur Web.

# Pour en savoir plus

L'intégration et la livraison continue sont à la base du mouvement ![DevOps](https://fr.wikipedia.org/wiki/Devops)