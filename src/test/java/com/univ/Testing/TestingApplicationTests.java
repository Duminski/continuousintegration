package com.univ.Testing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestingApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testCreationCompte() {
		Compte compte = new Compte();
		float solde = compte.getSolde();
		Assert.assertTrue(solde == 0);
	}

	@Test
	public void testCrediter() {
		try {
			Compte compte = new Compte();
			compte.crediter(100);
			float solde = compte.getSolde();
			Assert.assertTrue(solde != 0);
		} catch(Exception e) {				// si on a une exception... 
			Assert.fail("Erreur credit");	// ... c'est une erreur
		}		
	}
	
	@Test(expected = Exception.class)			// exception attendue...	
	public void testDebiterInferieur() throws Exception {
		Compte compte = new Compte();
		float solde = compte.debiter(-10);		// ... on ne peut pas debiter -10
	}

	@Test(expected = Exception.class)			// exception attendue...	
	public void testDebiterSuperieur() throws Exception {
		Compte compte = new Compte();
		float solde = compte.debiter(2000);		// ... on ne peut pas debiter 2000
	}

	@Test(expected = Exception.class)			// exception attendue...	
	public void testSetSolde() throws Exception {
		Compte compte = new Compte();
		compte.setSolde(-10);
	}

	@Test				
	public void testDebiterOK() throws Exception {
		Compte compte = new Compte();
		compte.crediter(150);
		float solde = compte.debiter(40);
		Assert.assertTrue(solde == 0);
	}

	@Test(expected = Exception.class)			
	public void testCrediterException() throws Exception {
		Compte compte = new Compte();
		compte.crediter(-10);
	}

}
